<?php

namespace App\Controller;

use App\Service\LoginService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\{Request, Response};
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class LoginController
 * @package App\Controller
 */
class LoginController extends AbstractController
{
    /**
     * @Rest\Get("/login", name="login_get")
     *
     * @return Response
     */
    public function loginGet(): Response
    {
        return $this->render('login/get.html.twig', [
            'login_get' => 'get'
        ]);
    }

    /**
     * @Rest\Post("/", name="login_post")
     *
     * @param Request $request
     * @param LoginService $loginService
     *
     * @return Response
     */
    public function loginPost(Request $request, LoginService $loginService): Response
    {
        return $this->render('login/post.html.twig', [
            'login_post' => 'post'
        ]);
    }
}
